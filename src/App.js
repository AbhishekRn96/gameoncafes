import React, { Component } from 'react';
import './App.css';
import Header from './components/Nav/Header';
import Routes from './components/Nav/Routes';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/material-design-icons/iconfont/material-icons.css';

class App extends Component {

  render() {
    return (
      <body className="App">
        <Header />
        <Routes />
      </body>
    );
  }
}

export default App;
