const mode = "prod"

const config = {
  url : mode == "dev" ? "https://thegameonn.herokuapp.com/" : "https://gameoncafecontrol.herokuapp.com/",
  bookingurl : mode == "dev" ? "https://thegameonn.herokuapp.com/" : "https://gameonbookingcontrol.herokuapp.com/"
}

export default config;