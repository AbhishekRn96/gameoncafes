import React, { Component } from 'react';
import './style.css';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from 'react-select';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Book extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cafe: {},
      Date: new Date(),
      DateValue: moment(new Date()).format('MMM Do YYYY'),
      dates: [],
      data: [],
      timeArrayStart: [],
      timeArrayEnd: [],
      StartTime: '',
      EndTime: '',
      platform: '',
      available : '',
      totalCost : '',
      totalPrice : '',
      totalHrs : '',
      selectedpc: 1,
      show: false,
      alert : null,
      hint : true,
    }
    this.increase = this.increase.bind(this);
    this.decrease = this.decrease.bind(this);
    this.book = this.book.bind(this);
  }

  componentWillMount() {
    let arr = [];
    for (let i = 0; i <= 20; i++) {
      arr.push(new Date().setDate(new Date().getDate()+i));
    }
    let cafe = localStorage.getItem('cafeid');
    this.setState({
      dates : arr,
    })
    this.setState({startDate : new Date()});
    fetch(`${config.url}getCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData});
        let arrStart = [];
        let arrEnd = [];
        arrEnd.push({value: parseInt(resData.OpenTime)+0.5, label: parseInt(resData.OpenTime)+':30'})
        for(let i = parseInt(resData.OpenTime); i < parseInt(resData.CloseTime)+1; i++){
          if(i!==parseInt(resData.CloseTime)){
            arrStart.push({value: i, label: i+':00'});
            arrStart.push({value: i+0.5, label: i+':30'});
          }
          if(i!==parseInt(resData.OpenTime)){
            if(i===parseInt(resData.CloseTime)){
              arrEnd.push({value: i, label: i+':00'});
            }
            else{
              arrEnd.push({value: i, label: i+':00'});
              arrEnd.push({value: i+0.5, label: i+':30'});
            }
          }
        }
        this.setState({
          timeArrayStart : arrStart
        })
        this.setState({
          timeArrayEnd : arrEnd
        })
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }
  
  increase() {
    if(this.state.selectedpc + 1 <= this.state.available){
      this.setState({
        selectedpc : this.state.selectedpc + 1,
        totalCost : (this.state.selectedpc + 1) * this.state.totalPrice
      })
    }
  }

  decrease() {
    if(this.state.selectedpc - 1 > 0){
      this.setState({
        selectedpc : this.state.selectedpc - 1,
        totalCost : (this.state.selectedpc - 1) * this.state.totalPrice
      })
    }
  }

  async booking() {
    if(moment(new Date()).format('MMM Do YYYY') === this.state.DateValue) {
      let a = new Date();
      console.log(this.state.StartTime, a.getHours(), a.getMinutes())
      if(this.state.StartTime%1 == 0.5) {
        if((a.getHours() === this.state.StartTime-0.5 && a.getMinutes() >= 30)) {
          return;
        }
      } else {
        if(a.getHours() >= this.state.StartTime) {
          return;
        }
      }
    }
    if(this.state.EndTime > this.state.StartTime && (this.state.EndTime - this.state.StartTime) >= 1 && this.state.platform !== ''){
      this.setState({show : true, available: 0});
      if(this.state.platform === "PC") {
        try {
          let response = await fetch(`${config.bookingurl}bookingPC`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            CafeId : this.state.Cafe.CafeId,
            date : this.state.DateValue,
            start : this.state.StartTime,
            end : this.state.EndTime,
            available : this.state.Cafe.PC_nos.length
          }),
        })
        let responseJson = await response.json();
        console.log(responseJson);
        if (responseJson.available === 100 || responseJson.available == undefined) {
          this.setState({
            available : this.state.Cafe.PCs,
            totalPrice : (this.state.Cafe.CostPC * (this.state.EndTime - this.state.StartTime)) * 1,
            totalCost : (this.state.Cafe.CostPC * (this.state.EndTime - this.state.StartTime)) * 1,
            totalHrs : this.state.EndTime - this.state.StartTime,
            selectedpc: 1,
            show: false,
            })
        } else {
          this.setState({
            available : responseJson.available,
            totalPrice : (this.state.Cafe.CostPC * (this.state.EndTime - this.state.StartTime)) * 1,
            totalCost : (this.state.Cafe.CostPC * (this.state.EndTime - this.state.StartTime)) * 1,
            totalHrs : this.state.EndTime - this.state.StartTime,
            selectedpc: 1,
            show: false,
            })
        }
        } catch (err) { 
          console.log(err.message);
        }
      } else {
        try {
          let response = await fetch(`${config.bookingurl}bookingConsole`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            CafeId : this.state.Cafe.CafeId,
            date : this.state.DateValue,
            start : this.state.StartTime,
            end : this.state.EndTime,
            available : this.state.Cafe.Console_nos.length
          }),
        })
        let responseJson = await response.json();
        console.log(responseJson);
        if (responseJson.available == 100 || responseJson.available == undefined) {
          this.setState({
            available : this.state.Cafe.Consoles,
            totalPrice : (this.state.Cafe.CostConsole * (this.state.EndTime - this.state.StartTime)) * 1,
            totalCost : (this.state.Cafe.CostConsole * (this.state.EndTime - this.state.StartTime)) * 1,
            totalHrs : this.state.EndTime - this.state.StartTime,
            selectedpc: 1,
            show: false,
          })
        } else {
          this.setState({
            available : responseJson.available,
            totalPrice : (this.state.Cafe.CostConsole * (this.state.EndTime - this.state.StartTime)) * 1,
            totalCost : (this.state.Cafe.CostConsole * (this.state.EndTime - this.state.StartTime)) * 1,
            totalHrs : this.state.EndTime - this.state.StartTime,
            selectedpc: 1,
            show: false,
            })
        }
        } catch (err) { 
          console.log(err.message);
        }
      }
    }
  }

  async book() {
    this.setState({
      hint : false
    })
    if(this.state.platform == "PC") {
      try {
        let response = await fetch(`${config.bookingurl}bookPC`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Date : this.state.DateValue,
              DateTime : this.state.Date,
              Status : "Active",
              User : {Username : this.state.Cafe.Name, Email : this.state.Cafe.Email},
              Cafe : {CafeId : this.state.Cafe.CafeId, Cafename : this.state.Cafe.Name, CafePic : this.state.Cafe.Main},
              TotalSystems : this.state.selectedpc,
              AvailablePCs : this.state.available - this.state.selectedpc,
              StartTime : this.state.StartTime,
              EndTime : this.state.EndTime,
              TotalHours : this.state.totalHrs,
              TotalCost : this.state.totalCost + this.state.totalCost*0.05,
            }),
          })
          let responseJson = await response.json();
          if(responseJson.BookingId){
            this.setState({
              available : null,
              alert : 1,
              hint : true
            })
            setTimeout(() => this.setState({alert : null}), 3000);
          }
      } catch (err) { 
        console.log(err.message);
      }
    } else {
      try {
        let response = await fetch(`${config.bookingurl}bookConsole`, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Date : this.state.DateValue,
              DateTime : this.state.Date,
              Status : "Active",
              User : {Username : this.state.Cafe.Name, Email : this.state.Cafe.Email},
              Cafe : {CafeId : this.state.Cafe.CafeId, Cafename : this.state.Cafe.Name, CafePic : this.state.Cafe.Main},
              TotalSystems : this.state.selectedpc,
              AvailableConsoles : this.state.available - this.state.selectedpc,
              StartTime : this.state.StartTime,
              EndTime : this.state.EndTime,
              TotalHours : this.state.totalHrs,
              TotalCost : this.state.totalCost + this.state.totalCost*0.05,
            }),
          })
          let responseJson = await response.json();
          if(responseJson.BookingId){
            this.setState({
              available : null,
              alert : 1,
              hint : true
            })
            setTimeout(() => this.setState({alert : null}), 3000);
          }
      } catch (err) { 
        console.log(err.message);
      }
    }
  }

  handleChange(date) {
    this.setState({
      Date: date,
      DateValue: moment(date).format('MMM Do YYYY'),
      available : '',
    });
  }

  handleOptionsStart(event) {
    this.setState({
      StartTime : event.value,
      available : '',
    })
  }

  handleOptionsEnd(event) {
    this.setState({
      EndTime : event.value,
      available : '',
    })
  }

  render() {
    return(
      <div className="container-fluid" style={{marginTop:110}} >
        <div className="row" style={{marginLeft: '2%', marginTop: '10%'}}>
          <label style={{fontWeight: '500', color: 'grey'}}>Select Date:</label><br />
          <div style={{marginLeft: '2%'}}>
            <DatePicker
              dateFormat="MMM dd"
              selected={this.state.Date}
              onChange={this.handleChange.bind(this)}
              includeDates={this.state.dates}
              className="form-control"
              placeholderText="Select Date"
              fixedHeight
            />
          </div>
        </div>
        {this.state.timeArrayStart.length > 0 ? (
          <body style={{marginLeft: '2%', marginTop: '2%'}}>
            <div className='row' style={{marginTop: '2%', color: '#1C8ADB', fontWeight: '500'}}>
              <label style={{fontWeight: '500', color: 'grey', marginLeft: '1.5%'}}>Select Platform:</label>
              <div className='form-check' style={{marginLeft: '5%'}}>
                <input type='radio' id='platformPC' checked={this.state.platform === 'PC' ? true : false} className='form-check-inline' onClick={() => this.setState({platform : 'PC'})}/><label for='platformPC'>PC</label>
              </div>
              <div className='form-check' style={{marginLeft: '5%'}}>
                <input type='radio' id='platformConsole' checked={this.state.platform === 'Console' ? true : false} className='form-check-inline' onClick={() => this.setState({platform : 'Console'})}/><label for='platformConsole'>Console</label>
              </div>
            </div>
            <label style={{fontWeight: '500', color: 'grey', marginTop: '2%'}}>Select Timings:</label>
            <div>
              <div style={{width: '50%', marginTop: '2%'}}>
                <Select
                  required
                  options={this.state.timeArrayStart}
                  isSearchable
                  placeholder='Start Time'
                  onChange={this.handleOptionsStart.bind(this)}/>
              </div>
              <div style={{width: '50%', marginTop: '2%'}}>
                <Select
                  required
                  options={this.state.timeArrayEnd}
                  isSearchable
                  placeholder='End Time'
                  onChange={this.handleOptionsEnd.bind(this)}/>
                </div>
            </div>
            <div className='row' style={{marginTop: '4%', marginLeft: '1.5%'}}>
              <button className='btn btn-primary' style={{borderRadius: 100}} onClick={() => this.booking()}>Show Available Systems</button>
              {this.state.show == false ? (
                <div style={{marginLeft: '3%', color: '#1C8ADB', fontSize: 25, fontWeight: '500'}}>
                  {this.state.available}
                </div>
              ) : (
                <div style={{marginLeft: '3%'}}>
                  <Digital color="#1C8ADB" size={30} speed={1} animating={true} />
                </div>
              )}  
            </div>
            {this.state.available > 0 ? (
              <div>
                <div className='row' style={{marginTop: '4%'}}>
                  <label style={{fontWeight: '500', color: 'grey', marginLeft: '1.5%'}}>Select No of Systems:</label>
                  <div className='text-select' style={{marginLeft: '5%'}} onClick={() => this.decrease()}>
                    <i className='material-icons' style={{color: '#1C8ADB', marginLeft: '2%', fontSize: 32, cursor: 'pointer'}}>remove_circle</i>
                  </div>
                  <div style={{marginLeft: '5%', color: '#1C8ADB', fontSize: 23, fontWeight: '500'}}>
                    {this.state.selectedpc}
                  </div>
                  <div className='text-select' style={{marginLeft: '2%'}} onClick={() => this.increase()}>
                    <i className='material-icons' style={{color: '#1C8ADB', fontSize: 32, cursor: 'pointer'}}>add_circle</i>
                  </div>
                </div>
                <div className='row' style={{marginTop: '4%'}}>
                  <p style={{fontWeight: '500', color: 'grey', marginLeft: '1.5%'}}>Systems Cost:</p>
                  <p style={{marginLeft: '5%', color: '#1C8ADB', fontSize: 23, fontWeight: '500'}}>&#8377;{this.state.totalCost}</p>
                </div>
                <div className='row'>
                  <p style={{fontWeight: '500', color: 'grey', marginLeft: '1.5%'}}>Convienience charges:</p>
                  <p style={{marginLeft: '5%', color: '#1C8ADB', fontSize: 23, fontWeight: '500'}}>&#8377;{this.state.totalCost*0.05}</p>
                </div>
                <div className='row'>
                  <p style={{fontWeight: '500', color: 'grey', marginLeft: '1.5%'}}>Total Payable:</p>
                  <p style={{marginLeft: '5%', color: '#1C8ADB', fontSize: 23, fontWeight: '500'}}>&#8377;{this.state.totalCost + this.state.totalCost*0.05}</p>
                </div>
                <button className='btn btn-primary' onClick={() => this.book()} style={{borderRadius: 5, marginBottom: '6%', marginLeft: '2%'}}>{this.state.hint ? "Book" : <Digital color="white" size={27} speed={1} animating={true} />}</button>
              </div>
            ) : null}
          </body>
        ) : (
          <center>
            <div style={{marginTop: '5%'}}>
              <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
            </div>
          </center>
        )}
        {this.state.alert !== null ? (
          <div className="alert alert-success" role="alert" style={{marginTop: '4%'}}>
            Booking Successful
          </div>
        ) : null}
      </div>
    )
  }
}

export default withRouter(Book);