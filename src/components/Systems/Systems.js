import React, { Component } from 'react';
import './style.css';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import SysLoad from  '../Home/SysLoad';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Systems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show : true,
      Cafe : {},
      PCs : [],
      Consoles : [],
      System : 0,
      Platform : null,      
    }
    this.togglePC = this.togglePC.bind(this);
    this.toggleConsole = this.toggleConsole.bind(this);
  }

  componentWillMount() {
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}getCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData});
        let arr = [1];
        let arr2 = [1];
        if(resData.PCs > 1) {
          for(let i = 2; i <= resData.PCs; i++) {
            arr.push(i);
          }
        }
        if(resData.Consoles > 1) {
          for(let i = 2; i <= resData.Consoles; i++) {
            arr2.push(i);
          }
        }
        this.setState({PCs : arr, Consoles : arr2, show : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  togglePC(digit) {
    this.setState({show : true});
    let arr = this.state.Cafe.PC_nos;
    if(arr.indexOf(digit) > -1) {
      arr.splice(arr.indexOf(digit), 1);
    } else {
      arr.push(digit);
    }
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}cafeSystems`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
        PC_nos : arr,
        Platform : 'PC'
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData, show : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  toggleConsole(digit) {
    this.setState({show : true});
    let arr = this.state.Cafe.Console_nos;
    if(arr.indexOf(digit) > -1) {
      arr.splice(arr.indexOf(digit), 1);
    } else {
      arr.push(digit);
    }
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}cafeSystems`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
        Console_nos : arr,
        Platform : 'Console'
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData, show : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  render() {
    return (
      <div className='container-fluid' style={{marginTop:100}}>
        <SysLoad Platform={this.state.Platform} System={this.state.System} />
        {this.state.show ? (
          <center>
            <div>
              <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
            </div>
          </center>
        ) : (
          <div style={{marginTop: '10%'}}>
            <div className='row'>
              {this.state.PCs.map((key, data) => (
                <div key={key} style={{marginLeft: '5%'}}>
                  {this.state.Cafe.PC_nos.indexOf(data+1) > -1 ? (
                    <div>
                      {this.state.System === data+1 && this.state.Platform === 'PC' ? (
                        <p style={{color : 'white', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: '#2f66bf', backgroundColor: '#2f66bf', borderRadius: 100}}>PC - {data+1}</p>
                      ) : (
                        <p style={{color : 'grey', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: 'white', borderRadius: 100}}>PC - {data+1}</p>
                      )}
                      <img src={require('../../images/PCOn.png')} alt="logo" style={{width: 160}} onClick={() =>{this.state.System === data+1 ? this.setState({Platform : null, System : 0}) : this.setState({Platform : 'PC', System : data+1})}}/>
                      <button className='btn btn-success' onClick={() => this.togglePC(data+1)}>Turn Off</button>
                    </div>
                  ) : (
                    <div>
                      {this.state.System === data+1 && this.state.Platform === 'PC' ? (
                        <p style={{color : 'white', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: '#2f66bf', backgroundColor: '#2f66bf', borderRadius: 100}}>PC - {data+1}</p>
                      ) : (
                        <p style={{color : 'grey', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: 'white', borderRadius: 100}}>PC - {data+1}</p>
                      )}
                      <img src={require('../../images/PCOff.png')} alt="logo" style={{width: 160}} onClick={() =>{this.state.System === data+1 ? this.setState({Platform : null, System : 0}) : this.setState({Platform : 'PC', System : data+1})}}/>
                      <button className='btn btn-danger' onClick={() => this.togglePC(data+1)}>Turn On</button>
                    </div>
                  )}
                </div>
               ))}
            </div>
            <div className='row' style={{marginTop : '4%'}}>
              {this.state.Consoles.map((key, data) => (
                <div key={key} style={{marginLeft: '5%'}}>
                  {this.state.Cafe.Console_nos.indexOf(data+1) > -1 ? (
                    <div>
                      {this.state.System === data+1 && this.state.Platform === 'Console' ? (
                        <p style={{color : 'white', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: '#2f66bf', backgroundColor: '#2f66bf', borderRadius: 100}}>Console - {data+1}</p>
                      ) : (
                        <p style={{color : 'grey', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: 'white', borderRadius: 100}}>Console - {data+1}</p>
                      )}
                      <img src={require('../../images/ConsoleOn.png')} alt="logo" style={{width: 160}} onClick={() =>{this.state.System === data+1 ? this.setState({Platform : null, System : 0}) : this.setState({Platform : 'Console', System : data+1})}}/>
                      <button className='btn btn-success' onClick={() => this.toggleConsole(data+1)}>Turn Off</button>
                    </div>
                  ) : (
                    <div>
                      {this.state.System === data+1 && this.state.Platform === 'Console' ? (
                        <p style={{color : 'white', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: '#2f66bf', backgroundColor: '#2f66bf', borderRadius: 100}}>Console - {data+1}</p>
                      ) : (
                        <p style={{color : 'grey', fontWeight: '500', fontSize: 18, padding: 10, borderWidth: 1, borderColor: 'white', borderRadius: 100}}>Console - {data+1}</p>
                      )}
                      <img src={require('../../images/ConsoleOff.png')} alt="logo" style={{width: 160}} onClick={() =>{this.state.System === data+1 ? this.setState({Platform : null, System : 0}) : this.setState({Platform : 'Console', System : data+1})}}/>
                      <button className='btn btn-danger' onClick={() => this.toggleConsole(data+1)}>Turn On</button>
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(Systems);