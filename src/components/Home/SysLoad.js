import React, { Component } from 'react';
import './style.css';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {withRouter} from 'react-router-dom';
import config from "../../config";
import Countdown from 'react-countdown-now';

class Sysload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      dates: [],
      data: [],
      cafe: "",
      loading: false,
      input: '',
      system : '',
      bookingId : null,
      userId : null,
      matchDate : null,
    };
    this.getBookings = this.getBookings.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.seatChange = this.seatChange.bind(this);
  }
  
  async componentDidMount() {
    let cafe = localStorage.getItem('cafeid');
    this.setState({
      dates : new Date().setDate(new Date().getDate()+20),
      cafe : cafe,
    })
    await this.setState({startDate : new Date()});
  }

  seatChange(Id, System) {
    this.setState({loading: true})
    fetch(`${config.url}systemChange`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        BookingId : Id,
        System : System,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.value.BookingId) {
        this.setState({input : ''})
        this.getBookings();
      }
    })
    .catch(err => {
      console.log("fail ",err);
      this.setState({loading: false})
    });
  }

  changeStatus(UserId, Id) {
    this.setState({loading: true})
    fetch(`${config.url}cancelBooking`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        BookingId : Id,
        UserId : UserId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message) {
        this.getBookings();
      }
    })
    .catch(err => {
      console.log("fail ",err);
      this.setState({loading: true})
    });
  }

  getBookings() {
    let cafe = localStorage.getItem('cafeid');
    this.setState({matchDate : this.state.startDate});
    if(this.props.System > 0 && this.props.Platform) {
      this.setState({loading: true, data : []})
      fetch(`${config.url}getBookingForSystem`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
        date : moment(this.state.startDate).format('MMM Do YYYY'),
        Platform : this.props.Platform,
        system : this.props.System,
      }),
      })
      .then(response =>  response.json())
      .then(resData => {
        if(resData.length > 0) {
          let arrBooked = [], arrCancelled = [], arrOngoing = [], arrUpcoming = [], date = new Date();
          for(let i = 0; i < resData.length; i++) {
            let st = resData[i].StartTime%1 === 0.5 ? resData[i].StartTime-0.2 : resData[i].StartTime;
            let et = resData[i].EndTime%1 === 0.5 ? resData[i].EndTime-0.2 : resData[i].EndTime;
            if(resData[i].Status === "Cancelled") {
              resData[i].status = "Cancelled";
              arrCancelled.push(resData[i]);
            } else if(st <= Number(date.getHours()+'.'+date.getMinutes()) && et >= Number(date.getHours()+'.'+date.getMinutes())) {
              resData[i].status = "Ongoing";
              arrOngoing.push(resData[i]);
            } else if(st > Number(date.getHours()+'.'+date.getMinutes())) {
              resData[i].status = "Upcoming";
              arrUpcoming.push(resData[i]);
            } else {
              resData[i].status = "Completed";
              arrBooked.push(resData[i]);
            }
          }
          arrOngoing.push.apply(arrOngoing, arrUpcoming);
          arrOngoing.push.apply(arrOngoing, arrBooked);
          arrOngoing.push.apply(arrOngoing, arrCancelled);
          this.setState({data : arrOngoing, loading : false});
        } else {
          this.setState({loading : false});
        }
      })
      .catch(err => {
        console.log("fail ",err);
      });
    }
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  render() {
    return(
      <div className="container-fluid" style={{marginTop:110}} >
        <div className="row" style={{marginLeft: '2%'}}>
          <DatePicker
            dateFormat="MMM dd"
            selected={this.state.startDate}
            onChange={this.handleChange.bind(this)}
            maxDate={this.state.dates}
            className="form-control"
            placeholderText="Select Date"
          />
          <button className="btn btn-primary" style={{marginLeft: '1%'}} onClick={() => this.getBookings()} disabled={this.state.startDate ? false : true}>
            Go
          </button>
        </div>
        <body style={{marginLeft: '2%', marginTop: '2%'}}>
          {this.state.loading ? (
            <center>
              <div style={{marginTop: '20%'}}>
                <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
              </div>
            </center>
          ) : this.state.data.length > 0 ? (
                this.state.data.map((item, key) => (
                  item.Status === "Cancelled" ? null : (
                  <div className='card shadow col-md-12' key={key} style={{marginBottom: 5, marginRight: 5}}>
                    <div className='card-body col-md-12'>
                      <div className="row">
                        <div className="col-sm-10">
                          <h6 className='heading'>{item.User.Username}</h6>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>BookingId : {item.BookingId}</span><br />
                          {this.state.input !== item.BookingId ? (
                            <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                              Systems : <span style={{color: '#2f66bf'}}>{item.BookedSystems.join()}</span>
                              <span onClick={async () => await this.setState({input : item.BookingId, system: item.BookedSystems.join()})}>
                                <i className='material-icons' style={{color: 'grey', marginLeft: '5%', cursor: 'pointer'}}>edit</i>
                              </span>
                            </span>
                          ) : (
                            <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                              Systems : <input type='text' style={{width: '50%'}} className='form-control' onChange={(event) => this.setState({system : event.target.value})} />
                              <span className='btn btn-primary' style={{marginTop: '0.5%', marginBottom: '0.5%'}} onClick={() => this.seatChange(item.BookingId, this.state.system)} >Ok</span>
                              <span className='btn btn-danger' style={{marginTop: '0.5%', marginLeft: '0.5%', marginBottom: '0.5%'}} onClick={() => this.setState({input : ''})}>Cancel</span>
                            </span>
                          )}<br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Paid : <span style={{color: 'green'}}>&#8377;{item.TotalCost}</span></span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Platform : {item.Platform}</span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                            <span>
                              <i className='material-icons' style={{color: '#4e6387', marginRight: '0.5%'}}>query_builder</i>
                            </span>
                            {item.StartTime%1 === 0.5 ? (item.StartTime-0.5+':30') : (item.StartTime+':00')} - {item.EndTime%1 === 0.5 ? (item.EndTime-0.5+':30') : (item.EndTime+':00')}
                          </span> - 
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>{item.TotalHours%1 === 0.5 ? (item.TotalHours-0.5+':30') : (item.TotalHours+':00')} Hrs</span><br />
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Systems booked : {item.TotalSystems}</span><br />
                          {moment(new Date()).format('MMM Do YYYY') === moment(this.state.matchDate).format('MMM Do YYYY') ?
                            item.StartTime%1 === 0.5 ? 
                              item.StartTime-0.5 > new Date().getHours() || (item.StartTime-0.5 == new Date().getHours() && new Date().getMinutes() < 30) ? (
                                <div classname="row">
                                  {item.Status !== "Cancelled" ? (
                                    <span style={{fontSize: 22, fontWeight: '500'}}>
                                      <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                    </span>
                                  ) : null}
                                </div>
                              ) : null :
                                item.StartTime > new Date().getHours() ? (
                                  <div classname="row">
                                    {item.Status !== "Cancelled" ? (
                                      <span style={{fontSize: 22, fontWeight: '500'}}>
                                        <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                      </span>
                                    ) : null}
                                  </div>
                                ) : null :
                            <div classname="row">
                              {item.Status !== "Cancelled" ? (
                                <span style={{fontSize: 22, fontWeight: '500'}}>
                                  <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId, userId : item.User.UserId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                </span>
                              ) : null}
                            </div>
                          }
                          <div className="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalLabel">Cancel Booking</h5>
                                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div className="modal-body">
                                  <p>Are you sure you want to cancel this booking with Id : {this.state.bookingId}</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger" onClick={() => this.changeStatus(this.state.userId, this.state.bookingId)}>Confirm</button>
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          {this.state.Status === "Ongoing" || item.status === "Ongoing" ? 
                            item.EndTime%1 === 0.5 ? (
                              <Countdown
                                onComplete={() => this.getBookings()}
                                date={new Date().setHours(item.EndTime-0.5, 30)} />
                            ) : (
                              <Countdown
                                onComplete={() => this.getBookings()} 
                                date={new Date().setHours(item.EndTime, 0)} />
                            )
                            : this.state.Status === "Upcoming" || item.status === "Upcoming" ? (
                                <span style={{color: 'orange', fontWeight: 500, padding: 10}}>Upcoming</span>
                          ) : this.state.Status === "Completed" || item.status === "Completed" ? (
                                <span style={{color: 'green', fontWeight: 500, padding: 10}}>Completed</span>
                          ) : this.state.Status === "Cancelled" || item.status === "Cancelled" ? (
                                <span style={{color: 'red', fontWeight: 500, padding: 10}}>Cancelled</span>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                  )
                ))
          ) : null}
        </body>
      </div>
    )
  }
}

export default withRouter(Sysload);