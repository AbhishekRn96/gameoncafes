import React, { Component } from 'react';
import './style.css';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {withRouter} from 'react-router-dom';
import Select from 'react-select';
import config from "../../config";
import Countdown from 'react-countdown-now';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: '',
      dates: [],
      data: [],
      data2: [],
      cafe: "",
      loading: true,
      Cafe: {},
      timeArrayStart: [],
      timeArrayEnd: [],
      StartTime: null,
      EndTime: null,
      bookingId: null,
      userId: null,
      matchDate : null,
      Status: null,
      cancelled : false,
    };
    this.getBookings = this.getBookings.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.filter = this.filter.bind(this);
    this.cancelled = this.cancelled.bind(this);
    this.completed = this.completed.bind(this);
    this.upcoming = this.upcoming.bind(this);
    this.ongoing = this.ongoing.bind(this);
    this.runBook = setInterval(() => this.getBookings(), 1800000);
  }

  componentWillMount() {
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}getCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData});
        let arrStart = [];
        let arrEnd = [];
        arrEnd.push({value: parseInt(resData.OpenTime)+0.5, label: parseInt(resData.OpenTime)+':30'})
        for(let i = parseInt(resData.OpenTime); i < parseInt(resData.CloseTime)+1; i++){
          if(i!==parseInt(resData.CloseTime)){
            arrStart.push({value: i, label: i+':00'});
            arrStart.push({value: i+0.5, label: i+':30'});
          }
          if(i!==parseInt(resData.OpenTime)){
            if(i===parseInt(resData.CloseTime)){
              arrEnd.push({value: i, label: i+':00'});
            }
            else{
              arrEnd.push({value: i, label: i+':00'});
              arrEnd.push({value: i+0.5, label: i+':30'});
            }
          }
        }
        this.setState({
          timeArrayStart : arrStart
        })
        this.setState({
          timeArrayEnd : arrEnd
        })
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });

  }
  
  async componentDidMount() {
    let cafe = localStorage.getItem('cafeid');
    this.setState({
      dates : new Date().setDate(new Date().getDate()+20),
      cafe : cafe,
    })
    await this.setState({startDate : new Date()});
    this.getBookings();
  }

  componentWillUnmount() {
    clearInterval(this.runBook);
  }

  changeStatus(UserId, Id) {
    this.setState({loading: true})
    fetch(`${config.url}cancelBooking`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        BookingId : Id,
        UserId : UserId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message) {
        this.setState({cancelled : true});
        this.getBookings();
        setTimeout(() => this.setState({cancelled : false}), 3000);
      }
    })
    .catch(err => {
      console.log("fail ",err);
      this.setState({loading: true})
    });
  }

  getBookings() {
    this.setState({loading: true, data : [], Status : null})
    this.setState({matchDate : this.state.startDate});
    fetch(`${config.url}getBookings`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cafeid : this.state.cafe,
        date : moment(this.state.startDate).format('MMM Do YYYY'),
      }),
    })
    .then(response =>  response.json())
    .then(async resData => {
      if(resData.length > 0) {
        let arrBooked = [], arrCancelled = [], arrOngoing = [], arrUpcoming = [], date = new Date();
        for(let i = 0; i < resData.length; i++) {
          let st = resData[i].StartTime%1 === 0.5 ? resData[i].StartTime-0.2 : resData[i].StartTime;
          let et = resData[i].EndTime%1 === 0.5 ? resData[i].EndTime-0.2 : resData[i].EndTime;
          if(resData[i].Status === "Cancelled") {
            resData[i].status = "Cancelled";
            arrCancelled.push(resData[i]);
          } else if(st <= Number(date.getHours()+'.'+date.getMinutes()) && et >= Number(date.getHours()+'.'+date.getMinutes())) {
            resData[i].status = "Ongoing";
            arrOngoing.push(resData[i]);
          } else if(st > Number(date.getHours()+'.'+date.getMinutes())) {
            resData[i].status = "Upcoming";
            arrUpcoming.push(resData[i]);
          } else {
            resData[i].status = "Completed";
            arrBooked.push(resData[i]);
          }
        }
        arrOngoing.push.apply(arrOngoing, arrUpcoming);
        arrOngoing.push.apply(arrOngoing, arrBooked);
        arrOngoing.push.apply(arrOngoing, arrCancelled);
        this.setState({data : arrOngoing, data2 : arrOngoing, loading : false});
      } else {
        this.setState({loading : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  filter() {
    this.setState({loading : true});
    let arr = this.state.data2;
    let arr2 = [];
    for(let i = 0; i < arr.length; i++) {
      if(arr[i].StartTime >= this.state.StartTime.value && arr[i].EndTime <= this.state.EndTime.value && arr[i].Status !== "Cancelled") {
        arr2.push(arr[i]);
      }
    }
    this.setState({data : arr2, loading : false});
  }

  cancelled() {
    this.setState({loading : true});
    let arr = this.state.data2;
    let arr2 = [];
    for(let i = 0; i < arr.length; i++) {
      if(arr[i].Status === "Cancelled") {
        arr2.push(arr[i]);
      }
    }
    this.setState({data : arr2, loading : false, Status : "Cancelled"});
  }

  completed() {
    this.setState({loading : true});
    let arr = this.state.data2;
    let arr2 = [];
    let date = new Date();
    for(let i = 0; i < arr.length; i++) {
      if(arr[i].EndTime%1 === 0.5 && (arr[i].EndTime-0.5 < date.getHours() || (arr[i].EndTime-0.5 === date.getHours() && date.getMinutes() > 30))) {
        arr2.push(arr[i]);
      } else if(arr[i].EndTime%1 !== 0.5 && (arr[i].EndTime < date.getHours() || (arr[i].EndTime === date.getHours() && date.getMinutes() > 0))) {
        arr2.push(arr[i]);
      }
    }
    this.setState({data : arr2, loading : false, Status : "Completed"});
  }

  upcoming() {
    this.setState({loading : true});
    let arr = this.state.data2;
    let arr2 = [];
    let date = new Date();
    for(let i = 0; i < arr.length; i++) {
      if(arr[i].StartTime%1 === 0.5 && (arr[i].StartTime-0.5 > date.getHours() || (arr[i].StartTime-0.5 === date.getHours() && date.getMinutes() < 30)) && arr[i].Status !== "Cancelled") {
        arr2.push(arr[i]);
      } else if(arr[i].StartTime%1 !== 0.5 && (arr[i].StartTime > date.getHours() || (arr[i].StartTime === date.getHours() && date.getMinutes() < 0)) && arr[i].Status !== "Cancelled") {
        arr2.push(arr[i]);
      }
    }
    this.setState({data : arr2, loading : false, Status : "Upcoming"});
  }

  ongoing() {
    this.setState({loading : true});
    let arr = this.state.data2;
    let arr2 = [];
    let date = new Date();
    for(let i = 0; i < arr.length; i++) {
      let st = arr[i].StartTime%1 === 0.5 ? arr[i].StartTime-0.2 : arr[i].StartTime;
      let et = arr[i].EndTime%1 === 0.5 ? arr[i].EndTime-0.2 : arr[i].EndTime;
      if(arr[i].Status !== "Cancelled" && st <= Number(date.getHours()+'.'+date.getMinutes()) && et >= Number(date.getHours()+'.'+date.getMinutes())) {
        arr2.push(arr[i]);
      } 
    }
    this.setState({data : arr2, loading : false, Status : "Ongoing"});
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  render() {
    return(
      <div className="container-fluid" style={{marginTop:110}} >
        <div className="row" style={{marginLeft: '2%'}}>
          <DatePicker
            dateFormat="MMM dd"
            style={{padding: 10}}
            selected={this.state.startDate}
            onChange={this.handleChange.bind(this)}
            className="form-control"
            placeholderText="Select Date"
            maxDate={this.state.dates}
            fixedHeight 
          />
          <button className="btn btn-primary" style={{marginLeft: '1%'}} onClick={() => this.getBookings()} disabled={this.state.startDate ? false : true}>
            Go
          </button>
          <button className="btn btn-white border-primary" style={{borderWidth: 2, marginLeft: '1%'}} data-toggle="modal" data-target="#exampleModal">
            <i className='material-icons text-primary'>filter_list</i>
          </button>
          <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Filter Bookings</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <body className="modal-body">
                  <div className="row" style={{marginLeft: '0.5%'}}>
                    <div style={{width: '30%'}}>
                      <Select
                        required
                        value={this.state.StartTime}
                        options={this.state.timeArrayStart}
                        isSearchable
                        onChange={(event) => this.setState({StartTime : event})}
                      />
                    </div>
                    <div style={{width: '30%', marginLeft: '2%'}}>
                      <Select
                        required
                        value={this.state.EndTime}
                        options={this.state.timeArrayEnd}
                        isSearchable
                        onChange={(event) => this.setState({EndTime : event})}
                      />
                    </div>
                    <button className="btn btn-primary" style={{marginLeft: '2%'}} onClick={() => this.filter()} disabled={this.state.StartTime && this.state.EndTime ? false : true}>
                      Go
                    </button>
                  </div>
                  <div style={{marginTop: '5%'}}>
                    <button className="btn btn-primary" onClick={() => this.upcoming()} data-dismiss="modal">
                      Upcoming
                    </button>
                    <button className="btn btn-warning" style={{marginLeft: '2%'}} onClick={() => this.ongoing()} data-dismiss="modal">
                      Ongoing
                    </button>
                  </div>
                  <div style={{marginTop: '2%'}}>
                    <button className="btn btn-success" onClick={() => this.completed()} data-dismiss="modal">
                      Completed
                    </button>
                    <button className="btn btn-danger" style={{marginLeft: '2%'}} onClick={() => this.cancelled()} data-dismiss="modal">
                      Cancelled
                    </button>
                  </div>
                  {/* <div className="dropdown" style={{marginLeft: '1%', padding: 10, paddingRight: -10}}>
                    <button className="btn btn-white dropdown-toggle border-primary text-primary" style={{borderWidth: 2}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Show Bookings
                    </button>
                    <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" data-dismiss="modal">
                      <a className="dropdown-item text-info bold font-weight-bold">Upcoming</a>
                      <a className="dropdown-item text-warning font-weight-bold">Ongoing</a>
                      <a className="dropdown-item text-success font-weight-bold">Completed</a>
                      <a className="dropdown-item text-danger font-weight-bold">Cancelled</a>
                    </div>
                  </div> */}
                </body>
              </div>
            </div>
          </div>
        </div>
        <body style={{marginLeft: '2%', marginTop: '2%'}}>
          {this.state.Status ? (
            <div>
              <h3 style={{color: 'grey'}}>{this.state.Status}</h3><br />
            </div>
          ) : null}
          {this.state.loading ? (
            <center>
              <div style={{marginTop: '20%'}}>
                <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
              </div>
            </center>
          ) : this.state.data.length > 0 ? (
                this.state.data.map((item, key) => (
                  <div className='card shadow col-md-12' key={key} style={{marginBottom: 5, marginRight: 5}}>
                    <div className='card-body col-md-12'>
                      <div className="row">
                        <div className="col-sm-10">
                          <h6 className='heading'>{item.User.Username}</h6>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>BookingId : {item.BookingId}</span><br />
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                            Systems : <span style={{color: '#2f66bf'}}>{item.BookedSystems.join()}</span>
                          </span><br />
                          {item.Status === "Cancelled" ? (
                            <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Status : <span style={{color : 'red'}}>Cancelled</span><br /></span>
                          ) : null}
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Paid : <span style={{color: 'green'}}>&#8377;{item.TotalCost+item.GCreditsUsed}</span></span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Platform : {item.Platform}</span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                            <span>
                              <i className='material-icons' style={{color: '#4e6387', marginRight: '0.5%'}}>query_builder</i>
                            </span>
                            {item.StartTime%1 === 0.5 ? (item.StartTime-0.5+':30') : (item.StartTime+':00')} - {item.EndTime%1 === 0.5 ? (item.EndTime-0.5+':30') : (item.EndTime+':00')}
                          </span> - 
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>{item.TotalHours%1 === 0.5 ? (item.TotalHours-0.5+':30') : (item.TotalHours+':00')} Hrs</span><br />
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Systems booked : {item.TotalSystems}</span><br />
                          {moment(new Date()).format('MMM Do YYYY') === moment(this.state.matchDate).format('MMM Do YYYY') ?
                            item.StartTime%1 === 0.5 ? 
                              item.StartTime-0.5 > new Date().getHours() || (item.StartTime-0.5 == new Date().getHours() && new Date().getMinutes() < 30) ? (
                                <div classname="row">
                                  {item.Status !== "Cancelled" ? (
                                    <span style={{fontSize: 22, fontWeight: '500', marginTop: '5%'}}>
                                      <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                    </span>
                                  ) : null}
                                </div>
                              ) : null :
                                item.StartTime > new Date().getHours() || (item.StartTime == new Date().getHours() && new Date().getMinutes() < 0) ? (
                                  <div classname="row">
                                    {item.Status !== "Cancelled" ? (
                                      <span style={{fontSize: 22, fontWeight: '500'}}>
                                        <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId, userId : item.User.UserId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                      </span>
                                    ) : null}
                                  </div>
                                ) : null :
                            <div classname="row">
                              {item.Status !== "Cancelled" ? (
                                <span style={{fontSize: 22, fontWeight: '500'}}>
                                  <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId, userId : item.User.UserId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                </span>
                              ) : null}
                            </div>
                          }
                          <div className="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalLabel">Cancel Booking</h5>
                                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div className="modal-body">
                                  <p>Are you sure you want to cancel this booking with Id : {this.state.bookingId}</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal" onClick={() => this.changeStatus(this.state.userId, this.state.bookingId)}>Confirm</button>
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          {this.state.Status === "Ongoing" || item.status === "Ongoing" ? 
                            item.EndTime%1 === 0.5 ? (
                              <Countdown
                                onComplete={() => this.getBookings()}
                                date={new Date().setHours(item.EndTime-0.5, 30)} />
                            ) : (
                              <Countdown
                                onComplete={() => this.getBookings()} 
                                date={new Date().setHours(item.EndTime, 0)} />
                            )
                            : this.state.Status === "Upcoming" || item.status === "Upcoming" ? (
                                <span style={{color: 'orange', fontWeight: 500, padding: 10}}>Upcoming</span>
                          ) : this.state.Status === "Completed" || item.status === "Completed" ? (
                                <span style={{color: 'green', fontWeight: 500, padding: 10}}>Completed</span>
                          ) : this.state.Status === "Cancelled" || item.status === "Cancelled" ? (
                                <span style={{color: 'red', fontWeight: 500, padding: 10}}>Cancelled</span>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                ))
          ) : (
            <center>
              <div className="text-primary" style={{marginTop: '20%', fontSize: 20, fontWeight: '500'}}>No bookings for this Selection Criteria</div>
            </center>
          )}
          {this.state.cancelled ? (
            <div class="alert alert-success" role="alert" style={{top: 0, position: 'absolute', width: '100%', alignItem: 'center'}}>
              Booking No - {this.state.bookingId} has been successfully cancelled.
            </div>
          ) : null}
        </body>
      </div>
    )
  }
}

export default withRouter(Home);