import React, { Component } from 'react';
import { Switch, withRouter, Route} from 'react-router-dom';
import { Redirect } from 'react-router'
import Authenticate from '../Authenticate/Authenticate';
import Home from '../Home/Home';
import Login from '../Login/Login';
import Systems from '../Systems/Systems';
import Book from '../Book/Book';
import Cafe from '../Cafe/Cafe';
import EditCafe from '../EditCafe/EditCafe';
import Dates from '../Dates/Dates';

class Routes extends Component {

  render() {
    return(
      <Switch>
        <Route exact path='/' render={() => localStorage.getItem('cafeid') ? <Home /> : <Login />}/>
        <Route exact path='/otp' render={() => localStorage.getItem('cafeid') ? <Home /> : <Authenticate />}/>
        <Route exact path='/home' render={() => localStorage.getItem('cafeid') ? <Home /> : <Redirect to="/"/>}/>
        <Route exact path='/systems' render={() => localStorage.getItem('cafeid') ? <Systems /> : <Redirect to="/"/>}/>
        <Route exact path='/book' render={() => localStorage.getItem('cafeid') ? <Book /> : <Redirect to="/"/>}/>
        <Route exact path='/cafe' render={() => localStorage.getItem('cafeid') ? <Cafe /> : <Redirect to="/"/>}/>
        <Route exact path='/editCafe' render={() => localStorage.getItem('cafeid') ? <EditCafe /> : <Redirect to="/"/>}/>
        <Route exact path='/dates' render={() => localStorage.getItem('cafeid') ? <Dates /> : <Redirect to="/"/>}/>
      </Switch>
    )
  }
}

export default withRouter(Routes);