import React, { Component } from 'react';
import './style.css'
import { Link, withRouter } from 'react-router-dom';

class Header extends Component {
  constructor(props) {
    super(props);
    this.Logout = this.Logout.bind(this);
  }

  Logout() {
    localStorage.clear();
    this.props.history.push('/');
  }

  render() {
    return (
      localStorage.getItem('cafeid') ? (
        <div>
          <nav className="navbar navbar-expand-sm bg-light navbar-light shadow fixed-top">
            <div className='container-fluid'>
              <Link className="navbar-brand" to='/home'>
                <img src={require('../../images/Cafe.png')} alt="logo" style={{width: 180}}/>
              </Link>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                {localStorage.getItem('cafeid') ? (
                  <ul className="navbar-nav">
                    <li className='nav-item'>
                      <Link className='nav-link' style={{fontSize : 17, fontWeight: '500'}} to='/home'>Bookings</Link>
                    </li>
                    <li className='nav-item'>
                      <Link className='nav-link' style={{fontSize : 17, fontWeight: '500'}} to='/systems'>Systems</Link>
                    </li>
                  </ul>
                ) : (
                  null
                )}
                {localStorage.getItem('cafeid') ? (
                  <ul className="navbar-nav ml-auto">
                    <li className='nav-item'>
                      <li className='nav-item dropdown'>
                        <Link className='nav-link dropdown-toggle' id='navbardrop' to='#' style={{fontSize: 17, fontWeight: '500'}} data-toggle='dropdown'>{localStorage.getItem('cafeid')}</Link>
                        <div className='dropdown-menu'>
                          <Link className='dropdown-item' style={{fontSize : 17, fontWeight: '500'}} to='/book'>Book</Link>
                          <Link className='dropdown-item' style={{fontSize : 17, fontWeight: '500'}} to='/cafe'>My Cafe</Link>
                          <div class="dropdown-divider"></div>
                          <Link className='dropdown-item' style={{fontSize: 17, fontWeight: '500'}} to='#' onClick={this.Logout}>Log Out</Link>
                        </div>
                      </li>
                    </li>
                  </ul>
                ) : (
                  null
                )}
              </div>
            </div>
          </nav>
        </div>
      ) : null
    )
  }
}

export default withRouter(Header);