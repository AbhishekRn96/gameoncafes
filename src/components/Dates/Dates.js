import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import moment from 'moment';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './style.css';
import config from "../../config";
import Countdown from 'react-countdown-now';

class Dates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cafe: null,
      disabledArr : [],
      startDate : new Date(),
      dateArr : [],
      data: [],
      loading : false,
      bookingId: null,
      userId: null,
    }
    this.getBookings = this.getBookings.bind(this);
    this.disableDate = this.disableDate.bind(this);
    this.enableDate = this.enableDate.bind(this);
    this.getCafe = this.getCafe.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
  }

  componentWillMount() {
    this.getCafe()
  }

  getCafe() {
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}getCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        if(resData.DisabledDates !== undefined) {
          let arr = resData.DisabledDates, arr2 = [];
          for(let i = 0; i < arr.length; i++) {
            let nd = arr[i].replace('nd', '');
            let st = nd.replace('st', '');
            let rd = st.replace('rd', '');
            let th = rd.replace('th', '');
            arr2.push(new Date(th));
          }
          this.setState({Cafe : resData, disabledArr : arr2, dateArr : resData.DisabledDates});
          this.getBookings();
        } else {
          this.getBookings();
          this.setState({Cafe : resData});
        }
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  changeStatus(UserId, Id) {
    this.setState({loading: true})
    fetch(`${config.url}cancelBooking`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        BookingId : Id,
        UserId : UserId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message) {
        this.setState({cancelled : true});
        this.getBookings();
        setTimeout(() => this.setState({cancelled : false}), 3000);
      }
    })
    .catch(err => {
      console.log("fail ",err);
      this.setState({loading: true})
    });
  }

  getBookings() {
    this.setState({loading: true, data : []})
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}getBookings`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cafeid : cafe,
        date : moment(this.state.startDate).format('MMM Do YYYY'),
      }),
    })
    .then(response =>  response.json())
    .then(async resData => {
      if(resData.length > 0) {
        let arrBooked = [], arrCancelled = [], arrOngoing = [], arrUpcoming = [], date = new Date();
        for(let i = 0; i < resData.length; i++) {
          let st = resData[i].StartTime%1 === 0.5 ? resData[i].StartTime-0.2 : resData[i].StartTime;
          let et = resData[i].EndTime%1 === 0.5 ? resData[i].EndTime-0.2 : resData[i].EndTime;
          if(resData[i].Status === "Cancelled") {
            resData[i].status = "Cancelled";
            arrCancelled.push(resData[i]);
          } else if(st <= Number(date.getHours()+'.'+date.getMinutes()) && et >= Number(date.getHours()+'.'+date.getMinutes())) {
            resData[i].status = "Ongoing";
            arrOngoing.push(resData[i]);
          } else if(st > Number(date.getHours()+'.'+date.getMinutes())) {
            resData[i].status = "Upcoming";
            arrUpcoming.push(resData[i]);
          } else {
            resData[i].status = "Completed";
            arrBooked.push(resData[i]);
          }
        }
        arrOngoing.push.apply(arrOngoing, arrUpcoming);
        arrOngoing.push.apply(arrOngoing, arrBooked);
        arrOngoing.push.apply(arrOngoing, arrCancelled);
        this.setState({data : arrOngoing, data2 : arrOngoing, loading : false});
      } else {
        this.setState({loading : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  enableDate() {
    this.setState({Cafe : null})
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}enableDate`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
        Date : moment(this.state.startDate).format('MMM Do YYYY'),
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message !== "failed") {
        this.getCafe();
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  disableDate() {
    this.setState({Cafe : null})
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}disableDate`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
        Date : moment(this.state.startDate).format('MMM Do YYYY'),
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      console.log(resData);
      if(resData.message !== "failed") {
        this.getCafe();
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  async handleChangeStart(date) {
    await this.setState({
      startDate: date
    });
    this.getBookings();
  }

  render() {
    return(
      <div className="container-fluid" style={{marginTop:110}}>
        {this.state.Cafe ? (
          <div>
            <div className='row' style={{marginLeft: '5%'}}>
              <h5 style={{color : 'grey', alignSelf: 'center'}}>Please click on a date to Enable/Disable bussiness for that date</h5>
            </div>
            <div className='row' style={{marginTop: '1%', marginLeft: '5%'}}>
              <div>
                <DatePicker
                  selected={this.state.startDate}
                  dateFormat="MMM dd"
                  highlightDates={[{'daydisabled': this.state.disabledArr}]}
                  onChange={this.handleChangeStart.bind(this)}
                  className="form-control"
                  placeholderText="Select Start Date"
                  shouldCloseOnSelect={false}
                />
              </div>
              <div style={{marginLeft: '2%'}}>
                {this.state.dateArr.indexOf(moment(this.state.startDate).format('MMM Do YYYY')) === -1 ? (
                  <button className='btn btn-danger' data-toggle="modal" data-target="#disableDate">
                    Disable
                  </button>
                ) : (
                  <button className='btn btn-success' data-toggle="modal" data-target="#enableDate">
                    Enable
                  </button>
                )}
              </div>
            </div>
            {this.state.loading ? (
              <center>
                <div style={{marginTop: '20%'}}>
                  <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
                </div>
              </center>
          ) : this.state.data.length > 0 ? (
                this.state.data.map((item, key) => (
                  <body className='card shadow col-md-12' key={key} style={{marginBottom: 5, marginRight: 5, marginTop: 15}}>
                    <div className='card-body col-md-12'>
                      <div className="row">
                        <div className="col-sm-10">
                          <h6 className='heading'>{item.User.Username}</h6>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>BookingId : {item.BookingId}</span><br />
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                            Systems : <span style={{color: '#2f66bf'}}>{item.BookedSystems.join()}</span>
                          </span><br />
                          {item.Status === "Cancelled" ? (
                            <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Status : <span style={{color : 'red'}}>Cancelled</span><br /></span>
                          ) : null}
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Paid : <span style={{color: 'green'}}>&#8377;{item.TotalCost+item.GCreditsUsed}</span></span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Platform : {item.Platform}</span><br/>
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                            <span>
                              <i className='material-icons' style={{color: '#4e6387', marginRight: '0.5%'}}>query_builder</i>
                            </span>
                            {item.StartTime%1 === 0.5 ? (item.StartTime-0.5+':30') : (item.StartTime+':00')} - {item.EndTime%1 === 0.5 ? (item.EndTime-0.5+':30') : (item.EndTime+':00')}
                          </span> - 
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>{item.TotalHours%1 === 0.5 ? (item.TotalHours-0.5+':30') : (item.TotalHours+':00')} Hrs</span><br />
                          <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>Systems booked : {item.TotalSystems}</span><br />
                          {moment(new Date()).format('MMM Do YYYY') === moment(this.state.matchDate).format('MMM Do YYYY') ?
                            item.StartTime%1 === 0.5 ? 
                              item.StartTime-0.5 > new Date().getHours() || (item.StartTime-0.5 == new Date().getHours() && new Date().getMinutes() < 30) ? (
                                <div classname="row">
                                  {item.Status !== "Cancelled" ? (
                                    <span style={{fontSize: 22, fontWeight: '500'}}>
                                      <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                    </span>
                                  ) : null}
                                </div>
                              ) : null :
                                item.StartTime > new Date().getHours() || (item.StartTime == new Date().getHours() && new Date().getMinutes() < 0) ? (
                                  <div classname="row">
                                    {item.Status !== "Cancelled" ? (
                                      <span style={{fontSize: 22, fontWeight: '500'}}>
                                        <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                      </span>
                                    ) : null}
                                  </div>
                                ) : null :
                            <div classname="row">
                              {item.Status !== "Cancelled" ? (
                                <span style={{fontSize: 22, fontWeight: '500'}}>
                                  <button className="btn" data-toggle="modal" data-target="#cancelModal" onClick={() => this.setState({bookingId : item.BookingId, userId : item.User.UserId})} style={{backgroundColor: '#d32e2e', cursor: 'pointer', color: 'white', fontWeight: '500', fontSize: 15}}>Cancel</button>
                                </span>
                              ) : null}
                            </div>
                          }
                          <div className="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalLabel">Cancel Booking</h5>
                                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div className="modal-body">
                                  <p>Are you sure you want to cancel this booking with Id : {this.state.bookingId}</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger" data-dismiss="modal" onClick={() => this.changeStatus(this.state.userId, this.state.bookingId)}>Confirm</button>
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-2">
                          {this.state.Status === "Ongoing" || item.status === "Ongoing" ? 
                            item.EndTime%1 === 0.5 ? (
                              <Countdown
                                onComplete={() => this.getBookings()}
                                date={new Date().setHours(item.EndTime-0.5, 30)} />
                            ) : (
                              <Countdown
                                onComplete={() => this.getBookings()} 
                                date={new Date().setHours(item.EndTime, 0)} />
                            )
                            : this.state.Status === "Upcoming" || item.status === "Upcoming" ? (
                                <span style={{color: 'orange', fontWeight: 500, padding: 10}}>Upcoming</span>
                          ) : this.state.Status === "Completed" || item.status === "Completed" ? (
                                <span style={{color: 'green', fontWeight: 500, padding: 10}}>Completed</span>
                          ) : this.state.Status === "Cancelled" || item.status === "Cancelled" ? (
                                <span style={{color: 'red', fontWeight: 500, padding: 10}}>Cancelled</span>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </body>
                ))
          ) : (
            <center>
              <div className="text-primary" style={{marginTop: '20%', fontSize: 20, fontWeight: '500'}}>No bookings for this Date</div>
            </center>
          )}
          </div>
        ) : (
          <center>
            <div style={{marginTop: '20%'}}>
              <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
            </div>
          </center>
        )}
        <div className="modal fade" id="disableDate" tabindex="-1" role="dialog" aria-labelledby="disableDateLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Confirm to Disable this Date</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
              <p>Are you sure you want to disable the date : <span className='text-primary'>{moment(this.state.startDate).format('MMM Do YYYY')}</span> for bussiness?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onClick={() => this.disableDate()}>Confirm</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" id="enableDate" tabindex="-1" role="dialog" aria-labelledby="enableDateLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Confirm to Enable this Date</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>Are you sure you want to enable the date : <span className='text-primary'>{moment(this.state.startDate).format('MMM Do YYYY')}</span> for bussiness?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" onClick={() => this.enableDate()}>Confirm</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Dates);