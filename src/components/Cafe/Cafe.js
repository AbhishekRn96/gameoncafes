import React, { Component } from 'react';
import './style.css';
import { Digital  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import "react-datepicker/dist/react-datepicker.css";
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Cafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Cafe: {},
    }
    this.route = this.route.bind(this);
  }

  componentWillMount() {
    let cafe = localStorage.getItem('cafeid');
    fetch(`${config.url}getCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : cafe,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show: false,
        })
      } else {
        this.setState({Cafe : resData});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  route() {
    this.props.history.push({
      pathname : '/editCafe',
      state : this.state.Cafe
    });
  }

  render() {
    return(
      <body className="container-fluid" style={{marginTop:110}}>
        {this.state.Cafe.CafeId ? (
          <div>
            <div className='row' style={{justifyContent: 'space-between'}}>
              <h3 style={{color: 'grey', fontWeight: '500', marginLeft: '2%'}}>{this.state.Cafe.Name}</h3>
              <div className='row' style={{marginLeft: '5%'}}>
                <button className='btn btn-primary' onClick={() => this.props.history.push('dates')}>Manage Dates</button>
              </div>
              <span style={{ marginRight: '5%'}} onClick={() => this.route()}>
                <i className='material-icons' style={{color: '#1C8ADB', cursor: 'pointer'}}>edit</i>
              </span>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:30}}>
              <div className='card-body'>
                <div className='row' style={{justifyContent: 'space-between'}}>
                  <h4 style={{color: 'grey', fontWeight: '500'}}>Total Earnings : <span style={{color: '#1C8ADB'}}>&#8377;{this.state.Cafe.TotalEarnings == undefined ? 0 : this.state.Cafe.TotalEarnings}</span></h4>
                  <button className='btn btn-primary' disabled={this.state.Cafe.TotalEarnings == undefined || this.state.Cafe.TotalEarnings === 0}>Pay Out</button>
                </div>
              </div>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:10}}>
              <div className='card-body'>
                <h4 style={{color: 'grey', fontWeight: '500'}}>Contact Details</h4>
                <p>Email : <span>{this.state.Cafe.Email}</span></p>
                <p>Contact: <span>{this.state.Cafe.Contact.Contact_1}</span></p>
                {this.state.Cafe.Contact.Contact_2 !== " " ? (
                  <p>Contact 2 : <span>{this.state.Cafe.Contact.Contact_2}</span></p>
                ) : null}
              </div>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:10}}>
              <div className='card-body'>
                <h4 style={{color: 'grey', fontWeight: '500'}}>Bank Details</h4>
                <p>Bank : <span>{this.state.Cafe.BankDetails.Bank}</span></p>
                <p>Account Number : <span>{this.state.Cafe.BankDetails.Account}</span></p>
                <p>IFSC Code : <span>{this.state.Cafe.BankDetails.IFSC}</span></p>
              </div>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:10}}>
              <div className='card-body'>
                <h4 style={{color: 'grey', fontWeight: '500'}}>Systems</h4>
                <div className='row' style={{marginLeft: '0.4%'}}>
                  <p>No of PCs : <span>{this.state.Cafe.PCs}</span></p>
                  <p style={{marginLeft: '10%'}}>No of Consoles : <span>{this.state.Cafe.Consoles}</span></p>
                </div>
              </div>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:10}}>
              <div className='card-body'>
                <h4 style={{color: 'grey', fontWeight: '500'}}>Timings</h4>
                <div className='row' style={{marginLeft: '0.4%'}}>
                  <p>Opening : <span>{this.state.Cafe.OpenTime%1 === 0.5 ? (this.state.Cafe.OpenTime-0.5+':30') : (this.state.Cafe.OpenTime+':00')}</span></p>
                  <p style={{marginLeft: '10%'}}>Closing : <span>{this.state.Cafe.CloseTime%1 === 0.5 ? (this.state.Cafe.CloseTime-0.5+':30') : (this.state.Cafe.CloseTime+':00')}</span></p>
                </div>
              </div>
            </div>
            <div className='card col-sm-12 shadow center' style={{borderWidth: 2, marginTop:10, marginBottom:10}}>
              <div className='card-body'>
                <h4 style={{color: 'grey', fontWeight: '500'}}>System Cost per Hour</h4>
                <div className='row' style={{marginLeft: '0.4%'}}>
                  <p>Cost for a PC : <span>&#8377;{this.state.Cafe.CostPC}</span></p>
                  <p style={{marginLeft: '10%'}}>Cost for a Console : <span>&#8377;{this.state.Cafe.CostConsole}</span></p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <center>
            <div style={{marginTop: '20%'}}>
              <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
            </div>
          </center>
        )}
      </body>
    )
  }
}

export default withRouter(Cafe);