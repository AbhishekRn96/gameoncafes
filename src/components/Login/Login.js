import React, { Component } from 'react';
import './styles.css';
import { Spinner  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show : false,
      username : "",
      password : "",
    }
    this.Login = this.Login.bind(this);
  }

  Login(e) {
    this.setState({show : true})
    e.preventDefault();
    fetch(`${config.url}cafeLogin`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message != "failed") {
        localStorage.setItem('cafeid', resData.CafeId);
        this.props.history.push('home');
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({show : false})
    });
  }

  render() {
    return (
      <div className='container-fluid' style={{marginTop:100}}>
        <img src={require('../../images/Cafe.png')} alt="logo" style={{width: 230, marginRight: 200}}/>
        <center>
        <div className='card col-md-4 shadow center' style={{borderWidth: 2, marginTop: 20}}>
            <div className='card-body'>
              <p style={{color: 'grey', fontSize: 15}}>Enter your Username and Password to Login</p>
              <form className="form-group">
                <div>
                  <label></label>
                  <input type="Username or Email" placeholder="Username" className="input_LS" required onChange={(event) => this.setState({username : event.target.value})} />
                </div>
                <div>
                  <label></label>
                  <div>
                    <input type="password" placeholder="Password" className="input_LS" required onChange={(event) => this.setState({password : event.target.value})} />
                  </div>
                </div>
                <div style={{marginTop: 30}}>
                  {this.state.show ? (
                    <div style={{marginTop: 20}}>
                      <Spinner  color="#397fef" size={25} speed={1} animating={true} />
                    </div>
                  ) : (
                    <button className="btn-primary btn" disabled={this.state.username && this.state.password ? false : true} onClick={this.Login} style={{backgroundColor: '#1C8ADB', color: 'white', fontWeight: '500', width: '100%'}}>
                      LOGIN
                    </button>
                  )}
                </div>
              </form>
              <p style={{color: 'grey', fontSize: 15}}>Or login using <span style={{color: '#2E86C1', cursor: 'pointer', fontWeight: '500'}} onClick={() => this.props.history.push('otp')}>One Time Password</span></p>
            </div>
          </div>
        </center>
      </div>
    );
  }
}

export default withRouter(Login);