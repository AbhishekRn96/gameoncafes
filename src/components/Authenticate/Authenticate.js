import React, { Component } from 'react';
import './style.css';
import { Spinner  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Authenticate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show : false,
      show3 : false,
      Phone: '',
      OTP: '',
      show2: true,
      error1: false,
      error2: false,
      resent : false,
    }
    this.VerifyNumber = this.VerifyNumber.bind(this);
    this.ConfigOTP = this.ConfigOTP.bind(this);
    this.ResendOTP = this.ResendOTP.bind(this);
  }

  VerifyNumber(e) {
    e.preventDefault();
    this.setState({
      show : true
    })
    fetch(`${config.url}checkNumber`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        number: '+91'+this.state.Phone,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({
          show2 : false,
          show : false,
          timerInterval : true
        })
      } else {
        this.setState({
          error1 : true,
          show : false,
        })
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  ResendOTP() {
    this.setState({
      show : true
    })
    fetch(`${config.url}checkNumber`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        number: '+91'+this.state.Phone,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({
          show2 : false,
          show : false,
          timerInterval : true,
          resent : true,
        })
        setTimeout(() => this.setState({resent : false}), 3000);
      } else {
        this.setState({
          error1 : true,
          show : false,
        })
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  ConfigOTP(e) {
    e.preventDefault();
    this.setState({
      show3: true,
    })
    fetch(`${config.url}checkOTP`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        number: '+91'+this.state.Phone,
        OTP: this.state.OTP
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "failed") {
        this.setState({
          show3: false,
          error2 : true
        })
      } else {
        localStorage.setItem('cafeid', resData.cafeid);
        this.props.history.push('home');
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  render() {
    return (
      <div className='container-fluid' style={{marginTop:100}}>
        <img src={require('../../images/Cafe.png')} alt="logo" style={{width: 230, marginRight: 200}}/>
        <center>
          <div className='card col-sm-4 shadow center' style={{borderWidth: 2, marginTop: 20}}>
            {this.state.show2 ? (
              <div className='card-body'>
                <p style={{color: 'grey', fontSize: 15}}>Please Enter your 10 digit Registered phone number</p>
                <form className="form-group" style={{marginTop: 10}}>
                  <span>
                    <input type="number" placeholder="Ex: 0012457963" className="input" value={this.state.Phone} required onChange={(event) => this.setState({Phone : event.target.value, error1: false})} />
                  </span>
                  <div style={{marginTop: 10}}>
                    {this.state.error1 ? (
                      <p style={{color: '#f93131', fontSize: 15, fontWeight: '500', cursor: 'context-menu'}}>This number is not registered with us</p>
                    ) : (
                      <p style={{color: 'white', fontSize: 15, fontWeight: '500', cursor: 'context-menu'}}>This number is not registered with us</p>
                    )}
                  </div>
                  <div style={{marginTop: 30}}>
                    {this.state.show ? (
                      <div style={{marginTop: 20}}>
                        <Spinner color="blue" size={25} speed={1} animating={true} />
                      </div>
                    ) : (
                      <button className="btn-primary btn" disabled={this.state.Phone.length === 10 ? false : true} onClick={this.VerifyNumber} style={{backgroundColor: '#1C8ADB', color: 'white', fontWeight: '500', fontSize: 15, width: '100%'}}>
                        Login with One Time Password
                      </button>
                    )}
                  </div>
                </form>
                <p style={{color: 'grey', fontSize: 13}}>We’ll send a SMS with One Time Password to your Registered Mobile Number</p>
              </div>
            ) : (
              <div className='card-body'>
                <p style={{color: 'grey', fontSize: 15}}>Please Enter the One Time Password sent to your Registered Mobile Number</p>
                <form className="form-group" style={{marginTop: 10}}>
                  <div>
                    <input type="number" className="inputOTP" required onChange={(event) => this.setState({OTP : event.target.value, error2: false})} />
                  </div>
                  <div style={{marginTop: 20}} className="row">
                    <p style={{color: '#1C8ADB', fontWeight: '500', cursor: 'pointer', marginLeft: "5%"}} onClick={() => this.state.resent ? null : this.ResendOTP()}>Resend OTP</p>
                    {this.state.error2 ? (
                      <p style={{color: '#f93131', fontWeight: '500', cursor: 'context-menu', marginLeft: "42%"}}>Invalid OTP</p>
                    ) : (
                      <p style={{color: 'white', fontWeight: '500', cursor: 'context-menu', marginLeft: "42%"}}>Invalid OTP</p>
                    )}
                  </div>
                  <div style={{marginTop: 45}}>
                    {this.state.show3 ? (
                      <div style={{marginTop: 20}}>
                        <Spinner  color="#397fef" size={25} speed={1} animating={true} />
                      </div>
                    ) : (
                      <div className="row" style={{width: "80%"}}>
                        <span className="btn-light btn" onClick={() => this.setState({show2 : true, show: false, show3: false})} style={{ fontWeight: '500', fontSize: 15, width: '40%', borderColor: 'red', color: 'red'}}>
                          Back
                        </span>
                        <input type="submit" value="Login" className="btn-primary btn" disabled={this.state.OTP.length === 6 ? false : true} onClick={this.ConfigOTP} style={{backgroundColor: '#1C8ADB', color: 'white', fontWeight: '500', fontSize: 15, width: '40%', marginLeft: '20%'}}>
                        </input>
                      </div>
                    )}
                  </div>
                </form>
              </div>
            )}
          </div>
          {this.state.resent ? (
            <div class="alert alert-success" role="alert" style={{top: 0, position: 'absolute', width: '100%', alignSelf: 'center'}}>
              OTP resent!
            </div>
          ) : null}
        </center>
      </div>
    );
  }
}

export default withRouter(Authenticate);