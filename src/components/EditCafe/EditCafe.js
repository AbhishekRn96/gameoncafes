import React, { Component } from 'react';
import './style.css';
import Select from 'react-select';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class EditCafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Name : '',
      Vendor : '',
      OpenTime : '',
      CloseTime : '',
      CostPC : '',
      CostConsole : '',
      BankName : '',
      AccountNo: '',
      IFSCcode: '',
      Email: '',
      Contactno1: '',
      Contactno2: '',
      Games: '',
      data: [],
      PCs : '',
      Consoles : '',
      timeArrayStart: [],
      timeArrayEnd: [],
      disable: false
    }
    this.goBack = this.goBack.bind(this);
    this.getGames = this.getGames.bind(this);
    this.editCafe = this.editCafe.bind(this);
  }

  async componentWillMount() {
    // if(!localStorage.getItem('cafeid')) {
    //   this.props.history.push('/');
    // }
    this.getGames();
    let CafeData = this.props.location.state;
    await this.setState({
      Name : CafeData.Name,
      Vendor : CafeData.Vendor,
      Email : CafeData.Email,
      Contactno1 : CafeData.Contact.Contact_1,
      Contactno2 : CafeData.Contact.Contact_2,
      BankName : CafeData.BankDetails.Bank,
      AccountNo : CafeData.BankDetails.Account,
      IFSCcode : CafeData.BankDetails.IFSC,
      OpenTime : CafeData.OpenTime,
      CloseTime : CafeData.CloseTime,
      CostPC : CafeData.CostPC,
      CostConsole : CafeData.CostConsole,
      Games : CafeData.Games,
      PCs : CafeData.PCs,
      Consoles : CafeData.Consoles,
    })
    let arrStart = [];
    let arrEnd = [];
    arrEnd.push({value: parseInt(0)+0.5, label: parseInt(0)+':30'})
    for(let i = parseInt(0); i < parseInt(23)+1; i++){
      if(i!==parseInt(23)){
        arrStart.push({value: i, label: i+':00'});
        arrStart.push({value: i+0.5, label: i+':30'});
      }
      if(i!==parseInt(0)){
        if(i===parseInt(23)){
          arrEnd.push({value: i, label: i+':00'});
        }
        else{
          arrEnd.push({value: i, label: i+':00'});
          arrEnd.push({value: i+0.5, label: i+':30'});
        }
      }
    }
    this.setState({
      timeArrayStart : arrStart
    })
    this.setState({
      timeArrayEnd : arrEnd
    })
  }

  getGames() {
    fetch(`${config.url}getGames`)
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length !== 0) {
        this.setState({data : resData})
        let arr = [];
        this.state.data.map((item, key) => arr.push({value : item.Game, label: item.Game, url: item.Url}))
        this.setState({data : arr})
      }
    })
    .catch(err => console.log(err));
  }

  handleOptions(event) {
    this.setState({
      Games : event
    })
  }

  editCafe() {
    this.setState({disable : true});
    let CafeData = this.props.location.state;
    let arr = [];
    let arr2 = [];
    if(this.state.PCs > 0) {
      for(let i = 1; i <= this.state.PCs; i++) {
        arr.push(i);
      }
    }
    if(this.state.Consoles > 0) {
      for(let i = 1; i <= this.state.Consoles; i++) {
        arr2.push(i);
      }
    }
    fetch(`${config.url}editCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Name : this.state.Name,
        Vendor : this.state.Vendor,
        Email: this.state.Email,
        Contact: {Contact_1: this.state.Contactno1, Contact_2: this.state.Contactno2},
        BankDetails: {Bank: this.state.BankName, Account: this.state.AccountNo, IFSC: this.state.IFSCcode},
        Main : CafeData.Main,
        Slides : CafeData.Slides,
        S3FolderDetails : {S3Folder : CafeData.S3Folder, TotalImages : CafeData.TotalImages, ImageExtension : CafeData.ImageExtension},
        Desc : CafeData.Desc,
        OpenTime : Number(this.state.OpenTime),
        CloseTime : Number(this.state.CloseTime),
        PCs: this.state.PCs,
        PC_nos: arr,
        Consoles: this.state.Consoles,
        Console_nos: arr2, 
        Location : CafeData.Location,
        Address : CafeData.Address,
        CostPC : Number(this.state.CostPC),
        CostConsole : Number(this.state.CostConsole),
        Latitude : Number(CafeData.Latitude),
        Longitude : Number(CafeData.Longitude),
        Games: this.state.Games,
        CafeId : CafeData.CafeId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.n === 1 && resData.nModified === 1) {
        this.setState({disable : false});
        this.props.history.push('/cafe');
      }
    })
    .catch(err => {
      this.setState({disable : false});
      console.log(err);
    });
  }

  goBack(){
    this.props.history.goBack();
  }

  handleOptionsStart(event) {
    this.setState({
      OpenTime : event.value,
    })
  }

  handleOptionsEnd(event) {
    this.setState({
      CloseTime : event.value,
    })
  }

  render() {
    return(
      <body className="container-fluid" style={{marginTop:110}}>
        <h3 style={{fontWeight: '500', color: 'grey'}}>Edit Cafe Details</h3>
        <form className='form-group' name='formCheck' onSubmit={this.editCafe}>
          <div className='placement'>
            <label>Cafe Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Name : event.target.value})} value={this.state.Name} />
          </div>
          <div className='placement'>
            <label>Vendor Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Vendor : event.target.value})} value={this.state.Vendor} />
          </div>
          <div className='placement'>
            <label>Email Address</label>
            <input type='email' required className='form-control' onChange={(event) => this.setState({Email : event.target.value})} value={this.state.Email} />
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Contact Info</label>
            </div>
            <span>
              <input type='text' required pattern='[0-9]{10}' onInvalid={(event) => event.target.setCustomValidity('Contact number should be of 10 digits')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Mobile Number 1' className='form-control-responsive' onChange={(event) => this.setState({Contactno1 : '+91'+event.target.value})} value={this.state.Contactno1.slice(3)} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <span>
              <input type='text' pattern='[0-9]{10}' onInvalid={(event) => event.target.setCustomValidity('Contact number should be of 10 digits')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Mobile Number 2' className='form-control-responsive' onChange={(event) => this.setState({Contactno2 : '+91'+event.target.value})} value={this.state.Contactno2.slice(3)} />
            </span>
          </div>
          <div className='placement'>
            <label>Bank Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({BankName : event.target.value})} value={this.state.BankName} />
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Bank Account Details</label>
            </div>
            <span>
              <input type='number' required placeholder='Account Number' className='form-control-responsive' onChange={(event) => this.setState({AccountNo : event.target.value})} value={this.state.AccountNo} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <span>
              <input type='text' required placeholder='IFSC Code' className='form-control-responsive' onChange={(event) => this.setState({IFSCcode : event.target.value})} value={this.state.IFSCcode} />
            </span>
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Timings</label>
            </div>
            <div className='row' style={{marginLeft: '0.5%'}}>
              <div style={{width: '40%'}}>
                <Select
                  required
                  value={this.state.OpenTime%1 === 0.5 ? [{value : this.state.OpenTime, label : (this.state.OpenTime-0.5)+':30'}] : [{value : this.state.OpenTime, label : (this.state.OpenTime)+':00'}]}
                  options={this.state.timeArrayStart}
                  isSearchable
                  placeholder='Start Time'
                  onChange={this.handleOptionsStart.bind(this)}/>
              </div>
              <span style={{marginLeft: '2%', marginRight: '2%'}}> - </span>
              <div style={{width: '40%'}}>
                <Select
                  required
                  value={this.state.CloseTime%1 === 0.5 ? [{value : this.state.CloseTime, label : (this.state.CloseTime-0.5)+':30'}] : [{value : this.state.CloseTime, label : (this.state.CloseTime)+':00'}]}
                  options={this.state.timeArrayEnd}
                  isSearchable
                  placeholder='End Time'
                  onChange={this.handleOptionsEnd.bind(this)}/>
              </div>
            </div>
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Available Systems</label>
            </div>
            <div>
             PC : &nbsp;<input type='number' placeholder='No of PCs' className='form-control-responsive' onChange={(event) => this.setState({PCs : event.target.value})} value={this.state.PCs} />
            </div>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <div>
             Consoles : &nbsp;<input type='number' placeholder='No of Consoles' className='form-control-responsive' onChange={(event) => this.setState({Consoles : event.target.value})} value={this.state.Consoles} />
            </div>
          </div>
          <div className='placement'>
            <label>Cost per Hour</label>
            <div>
              PC : &nbsp;&#8377;<input type='number' required placeholder='Cost per hour' className='form-control-responsive' onChange={(event) => this.setState({CostPC : event.target.value})} value={this.state.CostPC} />
            </div>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <div>
              Consoles : &nbsp;&#8377;<input type='number' required placeholder='Cost per hour' className='form-control-responsive' onChange={(event) => this.setState({CostConsole : event.target.value})} value={this.state.CostConsole} />
            </div>
          </div>
          <div className='placement'>
            <label>Select Games</label>
            <Select
              required
              value={this.state.Games}
              options={this.state.data}
              isMulti
              isSearchable
              isClearable
              onChange={this.handleOptions.bind(this)}/>
          </div>
          <div className='placement'>
            <span>
              <input type="submit" value="Save Changes" required disabled={this.state.disable} className='btn btn-info' />
            </span>
          </div>
        </form>
      </body>
    )
  }
}

export default withRouter(EditCafe);